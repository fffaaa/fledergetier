# fledergetier

Tool/experiment to convert pencil-written notes to crisp images, greatly improving readability.


The word "fledergetier" is a rather oldfashioned German word for the English "bat". It's derived from the edgy shape of the processed pencil lettering, flapping over the paper in dark grey.
