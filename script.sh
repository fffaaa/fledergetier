# ©2020 NP & FA — MIT
set -e

# vars
Folder=_construction_
Noteshrink=./noteshrink_/./noteshrink.py
Input=${1:?Please provide an input image} # input or error

# housekeeping
rm -rf $Folder/
mkdir $Folder/
cp $Input $Folder/00-original.jpg

# construction
cd $Folder
convert 00-original.jpg -morphology Erode Diamond:1 -morphology Open Ring:2 -morphology Erode Octagon:1 01-eroded.jpg
$Noteshrink 01-eroded.jpg -b 02-noteshrink
convert 02-noteshrink0000.png -contrast-stretch 87% 03-contrasted.jpg
convert 03-contrasted.jpg -morphology Open Diamond:2 04-opened.jpg
convert 04-opened.jpg -sharpen 0x10.0 05-sharpened.jpg
convert 05-sharpened.jpg -morphology Dilate Diamond 06-dilated.jpg
convert 01-eroded.jpg \( 06-dilated.jpg -negate \) -compose multiply -composite -auto-level 07-constrained.jpg
convert 07-constrained.jpg -negate 08-negated.jpg
convert 02-noteshrink0000.png 06-dilated.jpg -compose Multiply -gravity center -composite 09-composite-02-06.jpg

# display
cd ..
sxiv $Folder

# Todo erode square?
